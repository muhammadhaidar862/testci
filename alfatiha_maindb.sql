-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 02:01 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alfatiha_maindb`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `isi` longtext NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `id_kategori`, `judul`, `penulis`, `isi`, `tanggal`, `waktu`, `images`) VALUES
(9, 3, 'Ical dukung Jokowi, Agung Laksono makin panas', 'Admin', '<p><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Ketua penyelenggara rapat pimpinan nasional (Rapimnas) Golkar kubu&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/a/aburizal-bakrie/\">Aburizal Bakrie</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;(Ical),&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/n/nurdin-halid/\">Nurdin Halid</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">mengklaim kehadiran Menteri Hukum dan HAM&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/y/yasonna-h-laoly/\">Yasonna Laoly</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">dan Menko Polhukam&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/l/luhut-panjaitan/\">Luhut Pandjaitan</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;dalam pembukaan rapat pimpinan nasional (Rapimnas) Golkar pada Sabtu (23/1) lalu adalah bentuk pengakuan Pemerintah atas legal standing kepengurusan partai oleh kubu Bali.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Pemerintah sudah merestui&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/a/aburizal-bakrie/\">Aburizal Bakrie</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;dan Idrus Marham memimpin&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/p/partai-golongan-karya/\">Partai Golkar</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">,\" ucap Nurdin pada acara pembukaan Rapimnas Golkar di&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://www.merdeka.com/tag/j/jakarta/\">Jakarta</a></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;convention Center (JCC) Senayan, Senin (25/1).</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Tidak hanya itu, dalam rapimnas ini Golkar kubu Ical juga mendeklarasikan diri untuk mendukung pemerintahan&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/j/joko-widodo/\">Jokowi</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">-&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/m/muhammad-jusuf-kalla/\">JK</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">. Seperti diketahui, perpecahan yang terjadi di internal Golkar sejak awal adalah soal sikap politik partai yang mendukung atau berada di luar pemerintah.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Namun, Ketua Umum&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/p/partai-golongan-karya/\">Partai Golkar</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;hasil Munas Ancol,&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/a/agung-laksono/\">Agung Laksono</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;tidak sependapat. Agung menilai tidak bisa kehadiran perwakilan pemerintah di rapimnas langsung diklaim sebagai bentuk dukungan terhadap kepengurusan Golkar kubu Aburizal Bakrie.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Kehadiran Menkum HAM kami apresiasi untuk lihat secara langsung Rapimnas di sana. Dan apa yang disampaikan pemerintah itu hak pemerintah, tapi kami yakin itu bukan pengakuan,\" kata Agung.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Agung menepis pemerintah akui Golkar kubu Ical dan dia meyakini pemerintah tetap menjaga netralitas. Kehadiran Yasonna dan Luhut hanya untuk memenuhi undangan Rapimnas Golkar, tidak lebih dari itu.&nbsp;</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Kami berkeyakinan itu bukan berarti pemerintah berpihak ke sana,\" tepis Agung.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Selain itu, Agung mengkritik keras wacana perluasan wewenang Dewan Pertimbangan (Watim). Dirinya menuding ada agenda tersembunyi yang dilakukan Ical.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Memperkuat kewenangan Wantim buat apa? Yang diperkuat itu kaderisasi, persatuan kesatuan, jujur menjalankan kepentingan partai bukan pribadi bisnisnya,\" timpalnya.&nbsp;</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Menurutnya, perluasan wewenang harus melalui Musyawarah Nasional. Rapimnas yang digelar Golkar kubu Ical dianggapnya tidak memiliki wewenang mengubah kewenangan Dewan Pertimbangan Golkar dan seharusnya Rapimnas digelar untuk memperkuat kader partai.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Apalagi Rapimnas tidak ada kekuasaannya untuk mengubah kewenangan Wantim, harus lewat Munas, dan sebaiknya perkuat partai, hubungan pusat daerah, bagaimana konsisten jalankan ideologi partai, bukan kewenangan wantim diperkuat,\" bebernya.&nbsp;</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Tidak hanya itu, Agung juga menanggapi dingin hasil Rapimnas kubu Aburizal Bakrie yang sepakat menggelar Munaslub pada Juni 2016. Agung tetap ngotot agar Munaslub harus menyertakan tim transisi yang telah dibentuk Mahkamah Partai Golkar (MPG).&nbsp;</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Kami tidak percaya Munaslub. Lebih baik melalui tim transisi yang objektif. Kalau tidak, ada niat macam-macam yang ikut. Sehingga kita hasilkan keputusan yang betul-betul berdasarkan AD/ART,\" tegas Agung.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Pokoknya kami di sini mendorong Munas secepatnya dilakukan sesuai putusan MPG yaitu melalui tim transisi,\" imbuhnya.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Menurutnya, jika kubu Ical berkukuh tidak menyertakan tim transisi maka Munaslub tidak diakui pihaknya. \"Kalau tetap dilaksanakan Munaslub ya kami tidak mengakui,\" ujarnya.</span></p>', '2016-05-17', '01:50:04', '06_44_53_2016_01_26_Ical_dukung_Jokowi,_Agung_Laksono_makin_panas.jpg'),
(10, 3, 'Kriteria calon pendamping Ahok di Pilgub DKI', 'Admin', '<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Pemilihan Gubernur DKI&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://www.merdeka.com/tag/j/jakarta/\">Jakarta</a></strong>&nbsp;memang baru akan digelar tahun depan, namun panasnya persaingan sudah mulai dirasakan sejak tahun lalu. Beberapa nama digadang-gadang disiapkan untuk menantang petahana&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/b/basuki-tjahaja-purnama/\">Basuki Tjahaja Purnama</a></strong></strong>yang mengaku bakal bertarung melalui jalur independen.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Nama Wali Kota&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://www.merdeka.com/tag/s/surabaya/\">Surabaya</a></strong>&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/t/tri-rismaharini/\">Tri Rismaharini</a></strong></strong>&nbsp;alias Risma, Wali Kota Bandung&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/r/ridwan-kamil/\">Ridwan Kamil</a></strong></strong>, pengusaha&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/s/sandiaga-salahuddin-uno/\">Sandiaga Uno</a></strong></strong>, hingga politisi Golkar&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/t/tantowi-yahya/\">Tantowi Yahya</a></strong></strong>, diyakini bakal jadi pesaing kuat Ahok. Namun dari nama-nama tersebut, belum ada satupun yang memastikan bakal maju menantang Ahok.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Tak kalah menarik teka teki sosok pendamping Ahok sebagai calon wakil gubernur yang akan menemaninya bertarung di Pilgub tahun depan. Tentu saja Ahok punya kriteria khusus untuk calon pendampingnya</p>', '2016-01-26', '06:47:29', '06_47_29_2016_01_26_Kriteria_calon_pendamping_Ahok_di_Pilgub_DKI.jpg'),
(11, 1, 'JK sebut Jokowi senang Golkar dukung pemerintah', 'Admin', '<p><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Wakil Presiden&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/m/muhammad-jusuf-kalla/\">Jusuf Kalla</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;hadir dalam Rapimnas</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/p/partai-golongan-karya/\">Partai Golkar</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">, Senin (25/1). JK hanya tertawa saat ditanya para wartawan apakah kehadirannya tersebut sepengetahuan Presiden</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/j/joko-widodo/\">Jokowi</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">JK lantas menyatakan, jika urusan politik Presiden&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/j/joko-widodo/\">Jokowi</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">&nbsp;pasti mengetahuinya.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Saya kira kalau urusan begini pasti Pak jokowi tahu,\" kata JK sambil tertawa di JCC,&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://www.merdeka.com/tag/j/jakarta/\">Jakarta</a></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Menurutnya, Jokowi pastinya senang melihat&nbsp;</span><strong style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px; outline: none 0px; padding: 0px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://profil.merdeka.com/indonesia/p/partai-golongan-karya/\">Partai Golkar</a></strong></strong><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">kembali bersatu. Apalagi Golkar kini mendukung pemerintahan Jokowi-JK.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Inikan hubungan internal partai, Pak Jokowi pasti akan sangat senang untuk melihat kegiatan parpol yang lebih bersatu dan mendukung pemerintah,\" katanya.</span></p>', '2016-01-26', '06:48:09', '06_48_09_2016_01_26_JK_sebut_Jokowi_senang_Golkar_dukung_pemerintah.jpg'),
(12, 2, 'Setelah ARRC 2015, Yudhistira menatap MotoGP dengan Honda', 'Admin', '<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Ahmad Yudhistira, pembalap asal Kalimantan yang berhasil membawa harum nama Indonesia di ajang Asia Road Racing Championship (ARRC) 2015 akhirnya berlabuh ke Astra Honda Racing Team.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Sukses bercokol di posisi ketiga klasemen akhir ARRC 2015 bersama tim Manual Tech KYT Kawasaki ini memutuskan berpindah ke Honda untuk menatap ajang MotoGP.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">\"Terima kasih semuanya perjuangan belum berakhir, 2016 start from today. Welcome to the red colours #team33.\" ujar Yudhistira lewat akun sosial medianya.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Namun, tentu saja turun ke ajang balap motor paling bergengsi sedunia ini bukan hal yang mudah. Untuk langkah pertama, kemungkinan Yudhistira bakal turun di ajang FIM CEV 2016 dengan menggunakan motor CBR600RR.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\"><em style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\">Ahmad Yudhistira pindah ke Astra Honda Racing Team&nbsp;<strong style=\"border: 0px none; margin: 0px; outline: none 0px; padding: 0px; background: none 0px 0px repeat scroll transparent;\"><a style=\"border: 0px none; margin: 0px; outline: none medium; padding: 0px; color: #2b67a2; text-decoration: none; background: none 0px 0px repeat scroll transparent;\" href=\"http://www.merdeka.com/tag/f/facebook/\">Facebook</a></strong>.com/Haji.Ahmad.Yudhistira</em></p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Dengan andil besar Honda di gelaran Moto2 sebagai pemasok tunggal mesin di kelas tersebut, tak sulit bagi Yudhistira untuk berkiprah di Moto2 yang berada di bawah MotoGP. Besar kesempatan pembalap kelahiran Kota Banjarmasin ini untuk bergabung dengan tim Federal Oil Gresini. Dulunya tim ini juga pernah menggunakan pembalap Indonesia seperti Doni Tata dan Rafid Topan.</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Jika berhasil menorehkan prestasi mentereng di kancah Moto2, bukan tak mungkin tiga hingga empat tahun mendatang Yudhistira berhasil menjadi pembalap Indonesia pertama yang tampil di balap MotoGP.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"font-family: arial; border: 0px none; font-size: 15px; margin: 0px 0px 10px; outline: none 0px; padding: 0px 0px 10px; width: 630px; line-height: 25px; background-image: none; background-attachment: scroll; background-size: initial; background-origin: initial; background-clip: initial; background-position: 0px 0px; background-repeat: repeat;\">Terlebih lagi, mulai 2017 Indonesia bakal menjadi salah satu tuan rumah penyelenggaraan MotoGP di tiap musimnya.</p>', '2016-01-26', '06:56:29', '06_49_01_2016_01_26_Setelah_ARRC_2015,_Yudhistira_menatap_MotoGP_dengan_Honda.jpg'),
(13, 2, 'Memphis Depay kembali ke PSV Eindhoven', 'Admin', '<p><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Memphis Depay tak diturunkan sepanjang pertandingan ketika Manchester United menghadapi Southampton akhir pekan kemarin. Sehari kemudian, Depay berangkat ke Philips Stadium kandang PSV Eindhoven.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Kedatangan Depay di markas mantan klubnya, menurut Daily Mail, hanya untuk melihat mantan timnya bertanding melawan Twente FC. Selain itu, kunjungan ini juga sebagai kesempatan mengucapkan \"Goodbye\" kepada fans PSV yang telah mendukungnya selama ia bermain di sana.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Depay menyalami pendukung PSV Eindhoven di Philips Stadium</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Dalam kunjungannya tersebut, PSV sukses menggulung Twente dengan skor 4-2. Berbeda dengan MU yang justru kalah 0-1 dari Soton di kandang sendiri.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Depay sendiri sejak datang ke Old Trafford pada musim panas kemarin dengan harga 25 juta pounds belum memberikan dampak signifikan. Dari 28 pertandingan yang ia jalani, ia baru mengumpulkan lima gol. (dm/shd)</span></p>', '2016-01-26', '06:51:09', '06_51_09_2016_01_26_Memphis_Depay_kembali_ke_PSV_Eindhoven.jpg'),
(14, 1, 'Cerita tragis 2 Brimob tewas dilindas pencuri sawit', 'Admin', '<p><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Dua personel anggota Satuan Brimob Polda Sumatera Barat Brigadir Anasril (36) dan Brigadir Nanang Hariansyah (30) gugur dalam tugas. Keduanya tewas dilindas truk Dyna yang dikendarai Bagus Diantoro dan Anda Tandi, pencuri buah kelapa sawit milik PT Sumbar Andalas Kencana (SAK), Minggu (24/1) sekitar pukul 04.00 WIB.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Kabid Humas Polda Sumbar AKBP Syamsi mengatakan, peristiwa tersebut terjadi di Afdeling E6 Lokasi Perkebunan PT SAK Muaro Timpeh Kecamatan Padang Laweh, Kabupaten Dharmasraya.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Awalnya dua anggota Brimob yang memang di-BKO-kan di PT SAK itu hendak mengadang truk yang sudah diduga sebagai pencuri buah kelapa sawit. Namun, ketika itu pelaku malah menabrak kedua anggota,\" ujar Syamsi.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Kedua anggota Brimob tersebut mendapat tugas pengamanan di PT SAK, karena dapat laporan dari pimpinan kebun PT SAK Timur, Gunardi yang mendapat informasi dari Divisi Manager II Kasman bahwa telah terjadi pencurian buah kelapa sawit di lokasi tuangan kebun Afdeling E.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">Selanjutnya, Gunardi dan Kasman bersama dua orang personel Brimob Padang Panjang Brigadir Anasril dan Brigadir Nanang menuju lokasi dengan menggunakan Mobil Toyota Hilux BA 8841 BG.</span><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><br style=\"font-family: arial; font-size: 15px; line-height: 25px;\" /><span style=\"font-family: arial; font-size: 15px; line-height: 25px;\">\"Dalam perjalanan menuju ke TKP dilihat portal Afdeling E telah rusak dan dibuka paksa, beberapa saat kemudian dilihat dua unit truk bak kayu jenis Mitsubishi Canter dan Dina Ryno bermuatan buah kelapa sawit menuju ke arah datangnya Mobil dari PT SAK,\" kata Syamsi.</span></p>', '2016-01-26', '06:56:10', '06_56_10_2016_01_26_Cerita_tragis_2_Brimob_tewas_dilindas_pencuri_sawit.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `atomiccounter`
--

CREATE TABLE `atomiccounter` (
  `idCounter` int(11) NOT NULL,
  `nameCounter` varchar(60) NOT NULL,
  `codeCounter` varchar(20) NOT NULL,
  `maxCounter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atomiccounter`
--

INSERT INTO `atomiccounter` (`idCounter`, `nameCounter`, `codeCounter`, `maxCounter`) VALUES
(1, 'donasi', '0325', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Peristiwa'),
(2, 'Olahraga'),
(3, 'Politik');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id_kontak` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `isi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id_kontak`, `nama_lengkap`, `email`, `website`, `isi`) VALUES
(2, 'Rendy Permana', 'rendypermana153@mail.com', 'http://oligardan.blogspot.co.id', 'wah websitenya bagus'),
(4, 'Ustadz Rojikin', 'rojikin666metalhead@gmail.com', 'http://metalheadbang.com', 'luar biasa metal'),
(7, 'Muhammad', 'muhammad@gmail.com', 'http://muhammad.com', 'assalamualaikum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_merchant`
--

CREATE TABLE `tbl_admin_merchant` (
  `admin_merchant_id` int(10) NOT NULL,
  `user_mobile_id` int(10) NOT NULL,
  `merchant_id` int(10) DEFAULT NULL,
  `admin_merchant_nama` varchar(255) NOT NULL,
  `admin_merchant_nik` varchar(255) NOT NULL,
  `admin_merchant_no_hp` varchar(255) NOT NULL,
  `admin_merchant_jenis_kelamin` varchar(255) NOT NULL,
  `admin_merchant_tempat_lahir` varchar(255) NOT NULL,
  `admin_merchant_tanggal_lahir` date NOT NULL,
  `admin_merchant_alamat` varchar(1000) NOT NULL,
  `admin_merchant_facebook` varchar(255) NOT NULL,
  `admin_merchant_twitter` varchar(255) NOT NULL,
  `admin_merchant_instagram` varchar(255) NOT NULL,
  `foto_id` varchar(255) DEFAULT '',
  `foto_selfie` varchar(255) DEFAULT '',
  `admin_merchant_level` varchar(45) DEFAULT '',
  `registration_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin_merchant`
--

INSERT INTO `tbl_admin_merchant` (`admin_merchant_id`, `user_mobile_id`, `merchant_id`, `admin_merchant_nama`, `admin_merchant_nik`, `admin_merchant_no_hp`, `admin_merchant_jenis_kelamin`, `admin_merchant_tempat_lahir`, `admin_merchant_tanggal_lahir`, `admin_merchant_alamat`, `admin_merchant_facebook`, `admin_merchant_twitter`, `admin_merchant_instagram`, `foto_id`, `foto_selfie`, `admin_merchant_level`, `registration_date`, `update_date`) VALUES
(1, 18, 1, 'Admin Muhajirin', '1234567890123456', '082110527048', 'Pria', 'Manchester', '2000-09-08', 'Bogor', '', '', '', 'user_1_id.jpg', 'user_1_selfie.png', '', '2019-03-17 19:52:00', '2000-01-01 01:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_artikel`
--

CREATE TABLE `tbl_artikel` (
  `artikel_id` int(10) NOT NULL,
  `artikel_judul` varchar(255) NOT NULL,
  `artikel_gambar` varchar(255) NOT NULL,
  `artikel_isi` varchar(60000) NOT NULL,
  `artikel_tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `artikel_status` varchar(255) NOT NULL,
  `user_mobile_id` int(10) UNSIGNED NOT NULL,
  `artikel_category` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_artikel`
--

INSERT INTO `tbl_artikel` (`artikel_id`, `artikel_judul`, `artikel_gambar`, `artikel_isi`, `artikel_tanggal`, `artikel_status`, `user_mobile_id`, `artikel_category`) VALUES
(1, 'Wisata Murah Dan Diskon Ke Raja Ampat Papua', 'artikel_1_gbr.png', '<p><p>Wmuslimers Kali ini mari kita kenal Raja Ampat, banyak yg bilang kalau tempat ini seperti bagian kecil dari surga yang ada di dunia karena keindahannya.</p>\n\n<p>Dari Wikipedia, Kepulauan Raja Ampat merupakan rangkaian empat gugusan pulau yang berdekatan dan berlokasi di barat bagian Kepala Burung (Vogelkoop) Pulau Papua. Secara administrasi, gugusan ini berada di bawah Kabupaten Raja Ampat, Provinsi Papua Barat. Kepulauan ini sekarang menjadi tujuan para penyelam yang tertarik akan keindahan pemandangan bawah lautnya. Empat gugusan pulau yang menjadi anggotanya dinamakan menurut empat pulau terbesarnya, yaitu Pulau Waigeo, Pulau Misool, Pulau Salawati, dan Pulau Batanta.</p>\n\n<p>Kami punya paketnya juga lho Kak, caranya install app WISATAMUSLIM, di android atau IOS lalu search RAJA AMPAT. Setelah dapat klik berapa pembeliannya lalu jangan lupa masukkan kode kupon : OPENTRIP biar dapat potongan diskon.&nbsp;</p>\n\n<p>Cari tanggal sesuai yg tercantum yaa Kak, beli pesawat saat ada promo lalu beli paketnya di kami.</p>', '2019-03-16 22:18:00', 'Y', 1, 'info'),
(2, 'Tips Perlengkapan yang Harus dibawa Saat Umroh', 'artikel_2_gbr.jpg', '<p><p>Wmuslimers, kali ini kami akan membagikan tips apa yang sebaiknya dibawa agar umroh kita lebih khusyu. Seperti kita ketahui perjalanan umroh berbeda dengan perjalan wisata lainnya, apa sih yang buat beda??? Tentunya <strong>pahala ibadah</strong> di Masjidil Haram dan Nabawi yang sangat besar. <em>So</em>, yuk simak apa saja sih yang <strong>WAJIB DIBAWA</strong>, detailnya sebagai berikut:</p>\n\n<ul>\n	<li>Biasanya travel akan memberikan 2 koper, koper kabin dan koper bagasi.</li>\n	<li><strong>Koper kabin</strong> isi baju, kain ihram, sabun, pasta gigi kecil dan perlengkapan untuk beribadah ketika sampai di tanah suci. Sedangkan <strong>Koper besar</strong> diisi perlengkapan/baju ibadah sehari-hari dan sisanya masih ada space kosong, biasanya digunakan untuk membawa oleh oleh misal kurma dan kacang-kacangan.</li>\n	<li><strong>Bawa tas kecil dan kantong plastik</strong>, tas untuk menaruh semua alat komunikasi dan botol minum sedangkan kantong plastik untuk sandal. Tas inilah yang kita bawa saat ke masjid atau sedang tawaf umroh.</li>\n	<li>Jangan lupa pakai terus <strong>identitas/id card</strong> yang diberikan oleh travel agar jika kita nyasar cukup minta bantuan dengan menunjukkan kartu tersebut. Pernah satu waktu kami menemukan seorang tua yang terlepas dari rombongan, dengan ID Card yang tertempel maka orang tua tersebut dijemput oleh pembimbing dari travel.</li>\n</ul>', '2019-01-22 22:35:09', 'Y', 1, 'tips');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doa`
--

CREATE TABLE `tbl_doa` (
  `doa_id` int(10) NOT NULL,
  `doa_judul` varchar(255) NOT NULL,
  `foto_id` varchar(255) NOT NULL,
  `filename` varchar(45) DEFAULT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `doa_isi` varchar(60000) NOT NULL,
  `doa_tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `doa_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_doa`
--

INSERT INTO `tbl_doa` (`doa_id`, `doa_judul`, `foto_id`, `filename`, `media_type`, `doa_isi`, `doa_tanggal`, `doa_status`) VALUES
(1, 'Doa Sebelum Makan', 'doa_1_id.JPG', NULL, 'voice', 'Allahumma baarik llanaa fiima razaqtanaa waqinaa adzaa ban-naar', '2019-03-18 15:01:12', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_merchant`
--

CREATE TABLE `tbl_merchant` (
  `merchant_id` int(10) UNSIGNED NOT NULL,
  `merchant_mobile_id` int(10) UNSIGNED NOT NULL,
  `merchant_rate` int(10) UNSIGNED NOT NULL,
  `merchant_type` varchar(255) NOT NULL,
  `merchant_nama` varchar(255) NOT NULL,
  `merchant_nik` varchar(255) NOT NULL,
  `merchant_no_hp` varchar(255) NOT NULL,
  `merchant_alamat` varchar(1000) NOT NULL,
  `merchant_facebook` varchar(255) NOT NULL,
  `merchant_twitter` varchar(255) NOT NULL,
  `merchant_instagram` varchar(255) NOT NULL,
  `foto_id` varchar(255) NOT NULL,
  `foto_selfie` varchar(255) NOT NULL,
  `latitutde` varchar(45) NOT NULL,
  `longitude` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reciter`
--

CREATE TABLE `tbl_reciter` (
  `reciter_id` int(10) NOT NULL,
  `reciter_title` varchar(255) NOT NULL,
  `reciter_name` varchar(255) NOT NULL,
  `reciter_description` varchar(255) NOT NULL,
  `reciter_notes` varchar(255) NOT NULL,
  `file_type` varchar(25) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `foto_icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reciter`
--

INSERT INTO `tbl_reciter` (`reciter_id`, `reciter_title`, `reciter_name`, `reciter_description`, `reciter_notes`, `file_type`, `file_name`, `foto_icon`) VALUES
(1, 'Al-Fatihah', 'Abdullah Awad al-Juhani', 'Surat Al-Fatihah', '', 'mp3', '001_AbdullahAwadal-Juhani.mp3', ''),
(2, 'Al-Fatihah', 'Abdur-Rahman as-Sudais', 'Surat Al-Fatihah', '', 'mp3', '001_Abdur-Rahmanas-Sudais.mp3', ''),
(3, 'Al-Fatihah', 'Ali Abdur-Rahman al-Huthaify', 'Surat Al-Fatihah', '', 'mp3', '001_AliAbdur-Rahmanal-Huthaify.mp3', ''),
(4, 'Al-Fatihah', 'Baleela', 'Surat Al-Fatihah', '', 'mp3', '001_Baleela.mp3', ''),
(5, 'Al-Fatihah', 'Mishari Rashid al-`Afasy', 'Surat Al-Fatihah', '', 'mp3', '001_MishariRashidal-`Afasy.mp3', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekaman`
--

CREATE TABLE `tbl_rekaman` (
  `rekaman_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ustadz_id` int(10) UNSIGNED NOT NULL,
  `terapis_number` int(2) NOT NULL,
  `upload_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rekaman_name` varchar(55) NOT NULL,
  `rekaman_type` varchar(45) NOT NULL,
  `file_type` varchar(45) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `rekaman_status` varchar(55) DEFAULT 'open',
  `rekaman_star` varchar(5) DEFAULT '0',
  `rekaman_grade` varchar(45) DEFAULT '',
  `rekaman_note` text,
  `rekaman_grade_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rekaman`
--

INSERT INTO `tbl_rekaman` (`rekaman_id`, `user_id`, `ustadz_id`, `terapis_number`, `upload_date`, `rekaman_name`, `rekaman_type`, `file_type`, `file_name`, `rekaman_status`, `rekaman_star`, `rekaman_grade`, `rekaman_note`, `rekaman_grade_date`) VALUES
(5, 1, 1, 1, '2019-03-02 11:30:55', 'surat alfatihah', 'voice', 'MP3', '190302183055302_user_1_1_1_alfatihah_voice.mp3', 'close', '2', 'KURANG', 'Kurang. Ada banyak Tajwid yang belum sesuai', '2019-03-02 23:37:26'),
(6, 1, 1, 2, '2019-03-02 16:25:44', 'surat alfatihah', 'voice', 'MP3', '190302232544460_user_1_1_2_alfatihah_voice.mp3', 'close', '3.5', 'CUKUP BAIK', 'Alhamdulillah ada peningkatan. Masih Ada beberapa Tajwid yang belum sesuai', '2019-03-03 00:10:44'),
(9, 1, 1, 3, '2019-03-03 00:06:38', 'surat alfatihah', 'voice', 'MP3', '190303070638101_user_1_1_3_alfatihah_voice.mp3', 'open', '0', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `idTransaksi` bigint(20) NOT NULL,
  `frontend` varchar(15) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_mobile_type_name` varchar(45) DEFAULT NULL,
  `user_nama` varchar(60) NOT NULL,
  `billingCode` varchar(30) DEFAULT NULL,
  `dateTransaction` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `billAmount` varchar(10) DEFAULT NULL,
  `responseFlag` varchar(2) DEFAULT NULL,
  `kredit` int(11) DEFAULT NULL,
  `debit` int(11) DEFAULT NULL,
  `notes` varchar(800) DEFAULT NULL,
  `produk` varchar(20) DEFAULT NULL,
  `jenisProduk` varchar(20) DEFAULT NULL,
  `kodeProduk` varchar(20) DEFAULT NULL,
  `reffId` varchar(16) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `sender_mobile_type_name` varchar(45) DEFAULT NULL,
  `sender_nama` varchar(60) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `destination_mobile_type_name` varchar(45) DEFAULT NULL,
  `destination_nama` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`idTransaksi`, `frontend`, `user_id`, `user_mobile_type_name`, `user_nama`, `billingCode`, `dateTransaction`, `billAmount`, `responseFlag`, `kredit`, `debit`, `notes`, `produk`, `jenisProduk`, `kodeProduk`, `reffId`, `sender_id`, `sender_mobile_type_name`, `sender_nama`, `destination_id`, `destination_mobile_type_name`, `destination_nama`) VALUES
(1, 'Mobileapp', 1, 'admin_merchant', 'Admin Muhajirin', '', '2019-03-17 20:08:51', '123000', '1', 123000, 0, 'Donasi utk kegiatan masjid', 'DONASI', 'MERCHANT', '', '190317200851312', 1, 'user', 'Sapo Jarwo', 1, 'merchant', 'Masjid Al Muhajirin'),
(2, 'Mobileapp', 1, 'admin_merchant', 'Admin Muhajirin', '', '2019-03-17 20:09:53', '123000', '1', 123000, 0, 'Donasi utk kegiatan masjid', 'DONASI', 'MERCHANT', '', '190317200953061', 1, 'user', 'Sapo Jarwo', 1, 'merchant', 'Masjid Al Muhajirin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_mobile_id` int(10) UNSIGNED NOT NULL,
  `user_nama` varchar(255) NOT NULL,
  `user_nik` varchar(255) NOT NULL,
  `user_no_hp` varchar(255) NOT NULL,
  `user_jenis_kelamin` varchar(255) NOT NULL,
  `user_tempat_lahir` varchar(255) NOT NULL,
  `user_tanggal_lahir` date NOT NULL,
  `user_alamat` varchar(1000) NOT NULL,
  `user_facebook` varchar(255) NOT NULL,
  `user_twitter` varchar(255) NOT NULL,
  `user_instagram` varchar(255) NOT NULL,
  `foto_id` varchar(255) DEFAULT '',
  `foto_selfie` varchar(255) DEFAULT '',
  `user_level` varchar(45) DEFAULT '',
  `registration_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_mobile_id`, `user_nama`, `user_nik`, `user_no_hp`, `user_jenis_kelamin`, `user_tempat_lahir`, `user_tanggal_lahir`, `user_alamat`, `user_facebook`, `user_twitter`, `user_instagram`, `foto_id`, `foto_selfie`, `user_level`, `registration_date`, `update_date`) VALUES
(1, 1, 'Sapo Jarwo', '1234567890123456', '082110527047', 'Pria', 'Manchester', '2000-09-08', 'Bogor', '', '', '', 'user_1_id.jpg', 'user_1_selfie.png', '', '2019-03-02 16:57:29', '2019-03-03 07:47:27'),
(2, 3, 'Fulan Bin Fulan', '1234567890123456', '081122223333', 'Pria', 'Lamongan', '2000-09-08', 'Lamongan', '', '', '', 'user_2_id.jpg', 'user_2_selfie.png', '', '2001-01-01 00:00:00', '2001-01-01 00:00:00'),
(3, 7, 'Fulan 1 Bin Fulan1', '1234567890123456', '081234567890', 'Pria', 'Sumedang', '1999-09-19', 'Surabaya', 'fulanjaya', '', '', '190306103702092_user_3_id.jpg', '190306103620208_user_3_selfie.png', '', '2019-03-05 16:02:12', '2019-03-06 03:37:56'),
(4, 8, 'Roby Darmansyah', '', '085771011563', 'Pria', 'Jakarta', '1999-03-07', '', '', '', '', '', '', '', '2019-03-07 09:36:13', NULL),
(5, 9, 'A', '12345', '08123456789', 'Pria', 'B', '2009-03-08', 'ABC', '', '', '', '', '', '', '2019-03-08 08:33:30', NULL),
(6, 10, 'Bambang M', '123456', '087771628133', 'Pria', 'Jakarta', '2008-03-08', 'Jakarta', 'b_moelyono@yahoo.com', 'bambang', 'bambang', '', '', '', '2019-03-08 08:47:10', NULL),
(7, 11, 'Amrixs', '42424', '081388920387', 'Pria', '2', '1998-03-08', 'Sss', '', '', '', '', '', '', '2019-03-08 09:57:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_mobile`
--

CREATE TABLE `tbl_user_mobile` (
  `user_mobile_id` int(10) NOT NULL,
  `user_mobile_type_id` int(3) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_mobile_status` varchar(255) NOT NULL DEFAULT 'Y',
  `ftc_password` int(1) DEFAULT '0',
  `is_login` int(1) NOT NULL DEFAULT '0',
  `token_push_notif` varchar(200) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_mobile`
--

INSERT INTO `tbl_user_mobile` (`user_mobile_id`, `user_mobile_type_id`, `user_name`, `password`, `user_mobile_status`, `ftc_password`, `is_login`, `token_push_notif`) VALUES
(1, 1, '082110527047', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 1, 1, ''),
(2, 2, '085921297337', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, ''),
(3, 1, '081122223333', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, ''),
(4, 2, '085566667777', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 0, ''),
(7, 1, '081234567890', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, ''),
(8, 1, '085771011563', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, ''),
(9, 1, '08123456789', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, ''),
(10, 1, '087771628133', '753c660c449cb00bc000f1ad03fb9b7c', 'Y', 0, 1, ''),
(11, 1, '081388920387', 'e10adc3949ba59abbe56e057f20f883e', 'Y', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_mobile_type`
--

CREATE TABLE `tbl_user_mobile_type` (
  `user_mobile_type_id` int(10) UNSIGNED NOT NULL,
  `user_mobile_type_name` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_mobile_type`
--

INSERT INTO `tbl_user_mobile_type` (`user_mobile_type_id`, `user_mobile_type_name`, `table_name`) VALUES
(1, 'user', 'tbl_user'),
(2, 'ustadz', 'tbl_ustadz');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ustadz`
--

CREATE TABLE `tbl_ustadz` (
  `ustadz_id` int(10) UNSIGNED NOT NULL,
  `user_mobile_id` int(10) UNSIGNED NOT NULL,
  `ustadz_rate` int(10) UNSIGNED NOT NULL,
  `ustadz_name` varchar(255) NOT NULL,
  `ustadz_nik` varchar(255) NOT NULL,
  `user_no_hp` varchar(255) NOT NULL,
  `ustadz_jenis_kelamin` varchar(255) DEFAULT NULL,
  `ustadz_tempat_lahir` varchar(255) NOT NULL,
  `ustadz_tanggal_lahir` date NOT NULL,
  `ustadz_alamat` varchar(1000) NOT NULL,
  `ustadz_facebook` varchar(255) NOT NULL,
  `ustadz_twitter` varchar(255) NOT NULL,
  `ustadz_instagram` varchar(255) NOT NULL,
  `foto_id` varchar(255) NOT NULL,
  `foto_selfie` varchar(255) NOT NULL,
  `registration_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ustadz`
--

INSERT INTO `tbl_ustadz` (`ustadz_id`, `user_mobile_id`, `ustadz_rate`, `ustadz_name`, `ustadz_nik`, `user_no_hp`, `ustadz_jenis_kelamin`, `ustadz_tempat_lahir`, `ustadz_tanggal_lahir`, `ustadz_alamat`, `ustadz_facebook`, `ustadz_twitter`, `ustadz_instagram`, `foto_id`, `foto_selfie`, `registration_date`, `update_date`) VALUES
(1, 2, 5, 'Yusuf Manjur', '1234567890123456', '085921297337', 'Pria', 'Belawan', '1967-09-08', 'Jatiasih Bekasi', '', '', '', '', '', '2019-03-02 17:01:25', NULL),
(2, 4, 4, 'Ustadz Ok', '1234567890123456', '085566667777', 'Pria', 'Belawan', '1967-09-08', 'Jatiasih Bekasi', '', '', '', 'ustadz_2.id.jpg', 'ustadz_2_selfie.png', '2019-03-02 17:01:25', '2019-03-03 07:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

CREATE TABLE `tbl_video` (
  `video_id` int(11) NOT NULL,
  `judul_video` varchar(255) NOT NULL,
  `file_video` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` date NOT NULL,
  `pengupload` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(15) NOT NULL,
  `images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_lengkap`, `email`, `no_hp`, `alamat`, `username`, `password`, `level`, `images`) VALUES
(2, 'Lingga', 'admin@mail.com', '', 'jakarta', 'admin', '25d55ad283aa400af464c76d713c07ad', 'Administrator', '05_20_00_2019_03_10_Lingga.jpg'),
(3, 'angga', 'angga@mail.com', '1234567890', 'jakarta', 'angga', '25d55ad283aa400af464c76d713c07ad', 'Ustadz', '05_24_31_2019_03_10_angga.jpg'),
(4, 'Fahmi dafrin maulana', 'masapin68@gmail.com', '082335623028', 'bondowoso', 'dafrin', '25d55ad283aa400af464c76d713c07ad', 'AdminMerchant', ''),
(5, 'Fahreza Wisnu', 'reza@gmail.com', '083346782987', 'bondowoso', 'fahreza', '25d55ad283aa400af464c76d713c07ad', 'Ustadz', '');

-- --------------------------------------------------------

--
-- Table structure for table `versioncode`
--

CREATE TABLE `versioncode` (
  `idversionCode` int(11) NOT NULL,
  `versionCode` varchar(10) DEFAULT '',
  `versionApp` varchar(45) DEFAULT '',
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `versioncode`
--

INSERT INTO `versioncode` (`idversionCode`, `versionCode`, `versionApp`, `description`) VALUES
(1, '0', 'mobileapp', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `atomiccounter`
--
ALTER TABLE `atomiccounter`
  ADD PRIMARY KEY (`idCounter`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id_kontak`);

--
-- Indexes for table `tbl_admin_merchant`
--
ALTER TABLE `tbl_admin_merchant`
  ADD PRIMARY KEY (`admin_merchant_id`);

--
-- Indexes for table `tbl_artikel`
--
ALTER TABLE `tbl_artikel`
  ADD PRIMARY KEY (`artikel_id`);

--
-- Indexes for table `tbl_doa`
--
ALTER TABLE `tbl_doa`
  ADD PRIMARY KEY (`doa_id`);

--
-- Indexes for table `tbl_merchant`
--
ALTER TABLE `tbl_merchant`
  ADD PRIMARY KEY (`merchant_id`);

--
-- Indexes for table `tbl_reciter`
--
ALTER TABLE `tbl_reciter`
  ADD PRIMARY KEY (`reciter_id`);

--
-- Indexes for table `tbl_rekaman`
--
ALTER TABLE `tbl_rekaman`
  ADD PRIMARY KEY (`rekaman_id`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`idTransaksi`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_mobile`
--
ALTER TABLE `tbl_user_mobile`
  ADD PRIMARY KEY (`user_mobile_id`),
  ADD KEY `user_mobile_type_id` (`user_mobile_type_id`),
  ADD KEY `is_login` (`is_login`);

--
-- Indexes for table `tbl_user_mobile_type`
--
ALTER TABLE `tbl_user_mobile_type`
  ADD PRIMARY KEY (`user_mobile_type_id`);

--
-- Indexes for table `tbl_ustadz`
--
ALTER TABLE `tbl_ustadz`
  ADD PRIMARY KEY (`ustadz_id`);

--
-- Indexes for table `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `versioncode`
--
ALTER TABLE `versioncode`
  ADD PRIMARY KEY (`idversionCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `atomiccounter`
--
ALTER TABLE `atomiccounter`
  MODIFY `idCounter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_admin_merchant`
--
ALTER TABLE `tbl_admin_merchant`
  MODIFY `admin_merchant_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_artikel`
--
ALTER TABLE `tbl_artikel`
  MODIFY `artikel_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_doa`
--
ALTER TABLE `tbl_doa`
  MODIFY `doa_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_merchant`
--
ALTER TABLE `tbl_merchant`
  MODIFY `merchant_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_reciter`
--
ALTER TABLE `tbl_reciter`
  MODIFY `reciter_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_rekaman`
--
ALTER TABLE `tbl_rekaman`
  MODIFY `rekaman_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_user_mobile`
--
ALTER TABLE `tbl_user_mobile`
  MODIFY `user_mobile_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user_mobile_type`
--
ALTER TABLE `tbl_user_mobile_type`
  MODIFY `user_mobile_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_ustadz`
--
ALTER TABLE `tbl_ustadz`
  MODIFY `ustadz_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `versioncode`
--
ALTER TABLE `versioncode`
  MODIFY `idversionCode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
