
<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="card col-12">
    <div class="card-body login-card-body box-profile">
      <div class="text-center">
        <img class="profile-user-img img-fluid img-circle" src="<?=base_url()?>assets/img/logo.png" alt="User profile picture">
      </div>
    <h2 class="login-box-msg">Silahkan Login</h2>
    <div class="">
              <?= $this->session->flashdata('message');?>
    </div>
      <form action="<?=base_url()?>auth" method="post">
        <div class="form-group ">
          <p>Username</p>
          <input type="text" class="form-control" placeholder="Username..." name="username" id="username"  autocomplete="off" value="<?= set_value('username')?>">
          <?=form_error('username','<small class="text-danger pl-3">','</small>');?>
        </div>
        <div class="form-group ">
          <p>Kata sandi</p>
          <input type="password" class="form-control " placeholder="Password..." name="password" id="password">
          <?=form_error('password','<small class="text-danger pl-3">','</small>');?>  
        </div>
        <div class="row">
          <div class="col-4">
            <div class="form-group ">
            <button type="submit" class="btn btn-primary btn-block btn-flat ">Login</button>
          </div>
          </div>
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>

