
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
       
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <div class="input-group-append">
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?= base_url()?>assets/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url()?>assets/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $user['nama_lengkap']?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="<?=base_url()?>AdminMerchant" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Merchant
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url()?>AdminMerchant/daftarMerchant" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Daftar merchant</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url()?>AdminMerchant/infoAdminMerchant" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Info merchant</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>auth/logout" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>
                Keluar
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=$judul;?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        
          <div class="col-lg-12">
           <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Daftar Merchant</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">User Mobile</label>
                        <input type="number" class="form-control" id="exampleInputEmail1" placeholder="User Mobile">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Merchant</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Merchant">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">NIK</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="NIK">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">No Handphone</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="No Handphone" maxlength="12">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Jenis Kelamin</label>
                          <select class="form-control">
                            <option disabled selected>Jenis Kelamin</option>
                            <option>Laki-Laki</option>
                            <option>Perempuan</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Lahir</label>
                        <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Merchant">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Tempat Lahir</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Tempat Lahir">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Facebook</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Facebook">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Twitter</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Twitter">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Instagram</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Instagram">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Admin Merchant Level</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Admin Merchant Level">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Registration Date</label>
                          <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Registration Date">
                        </div>               
                      </div>     
                  </div>
                  <div class="col-lg-6">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Update Date</label>
                          <input type="date" class="form-control" id="exampleInputPassword1" placeholder="Update Date">
                        </div>               
                      </div>     
                  </div>
                  <div class="col-lg-12">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" placeholder="Alamat"></textarea>
                      </div>
                    </div>
                  </div>
                <div class="row">
                   
                </div>
                
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-alpha
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
