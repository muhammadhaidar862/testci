  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=$judul;?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        
          <div class="col-lg-12">
        <?php if ($this->session->flashdata('flash')) : ?>
          <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span> Data Siswa Berhasil <?=$this->session->flashdata('flash');?></span>
          </div>
        <?php endif ?>
           <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Daftar Merchant</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="" method="POST">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Admin Merchant Nama</label>
                        <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama">
                        <p class="form-text text-danger"><?= form_error('nama');?></p>
                      </div>
                    </div>
                  </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Merchant</label>
                        <input type="text" class="form-control" id="merchant" placeholder="Merchant" name="merchant">               
                        <p class="form-text text-danger"><?= form_error('merchant');?></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">NIK</label>
                        <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK">
                          <p class="form-text text-danger"><?= form_error('nik');?></p>
                      </div>
                    </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">No Handphone</label>
                        <input type="number" class="form-control" id="nohp" name="nohp" placeholder="No Handphone" maxlength="12">
                          <p class="form-text text-danger"><?= form_error('nohp');?></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Jenis Kelamin</label>
                          <select class="form-control">
                            <option disabled selected>Jenis Kelamin</option>
                            <option>Laki-Laki</option>
                            <option>Perempuan</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Lahir</label>
                        <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Merchant">
                          <p class="form-text text-danger"><?= form_error('tgl_lahir');?></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Tempat Lahir</label>
                        <input type="text" class="form-control" id="tmp_lahir" name="tmp_lahir" placeholder="Tempat Lahir">
                          <p class="form-text text-danger"><?= form_error('tmp_lahir');?></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Facebook</label>
                        <input type="text" class="form-control" id="fb" name="fb" placeholder="Facebook">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Twitter</label>
                        <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Instagram</label>
                        <input type="text" class="form-control" id="Instagram" name="instagram" placeholder="Instagram">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Admin Merchant Level</label>
                        <input type="text" class="form-control" id="adm_m_level" placeholder="Admin Merchant Level">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Registration Date</label>
                          <input type="date" class="form-control" id="registrasion_date" name="registrasion_date" placeholder="Registration Date">
                        </div>               
                      </div>     
                  </div>
                  <div class="col-lg-6">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Update Date</label>
                          <input type="date" class="form-control" id="update_date" name="update_date" placeholder="Update Date">
                        </div>               
                      </div>     
                  </div>
                  <div class="col-lg-12">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" placeholder="Alamat" id="alamat" name="alamat"></textarea>
                          <p class="form-text text-danger"><?= form_error('alamat');?></p>
                      </div>
                    </div>
                  </div>
                <div class="row">
                   
                </div>
                
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

