  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?=$judul;?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        
          <div class="col-lg-12">
           <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Form Input Artikel</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Judul</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Deskripsi</label>
                        <textarea class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="inputfile">File(.mp3)</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputfile">
                            <label class="custom-file-label" for="inputfile">Pilih File</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputPassword1">Nama Reciter</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Catatan</label>
                        <textarea class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="inputfoto">Foto</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputfoto">
                              <label class="custom-file-label" for="inputfoto">Pilih Foto</label>
                            </div>
                          </div>
                      </div>  
                    </div>
                  </div>
                </div>
                <div class="row">
                   
                </div>
                
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>

        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

