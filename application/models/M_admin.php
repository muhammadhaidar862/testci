<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
	public function tambahMerchant()
	{
		$data = [
			'admin_merchant_nama' => $this->input->post('nama',true),
			'admin_merchant_nik' => $this->input->post('nik',true),
			'admin_merchant_no_hp' => $this->input->post('nohp',true),
			'admin_merchant_tempat_lahir' => $this->input->post('tmp_lahir',true),
			'admin_merchant_tanggal_lahir' => $this->input->post('tgl_lahir',true),
			'admin_merchant_alamat' => $this->input->post('alamat',true)
		];
		$this->db->insert('tbl_admin_merchant',$data);
	}
	public function tambahArtikel()
	{
		$data = [
			'judul' 	=> $this->input->post('judul',true),
			'penulis' 	=> $this->input->post('penulis',true),
			'tanggal' 	=> $this->input->post('tanggal',true),
			'isi' 		=> $this->input->post('editor')
		];
		$this->db->insert('artikel',$data);
	}
}