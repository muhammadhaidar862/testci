<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_admin');
	}

	public function index()
	{
		$data['judul'] = 'Admin';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/index',$data);
		$this->load->view('templates/admin_footer');
	}
	public function artikel()
	{
		$data['judul'] = 'Artikel';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->form_validation->set_rules('judul','Judul','required|trim');
		$this->form_validation->set_rules('penulis','Penulis','required|trim');
		$this->form_validation->set_rules('tanggal','Tanggal','required');
		
		if ($this->form_validation->run() == FALSE) {
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/artikel',$data);
		$this->load->view('templates/admin_footer');			
		}else{
			$this->M_admin->tambahArtikel();
			$this->session->set_flashdata('flash' , 'Ditambahkan');
			redirect('Administrator/artikel');
		}
	}
	public function doa()
	{
		$this->form_validation->set_rules('judul','Judul','required|trim');
		$this->form_validation->set_rules('tanggal','Tanggal','required');

		$data['judul'] = 'Doa';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		if ($this->form_validation->run() ==FALSE ) {
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/doa',$data);
		$this->load->view('templates/admin_footer');			
		}else{
			$this->M_admin->upload();
			redirect('Administrator/doa');
		}

	}
	public function pengguna()
	{
		$data['judul'] = 'Pengguna';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/pengguna',$data);
		$this->load->view('templates/admin_footer');
	}
	public function daftarmerchant()
	{
		$data['judul'] = 'Daftar merchant';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->form_validation->set_rules('nama','Nama','required|trim');
		$this->form_validation->set_rules('nik','NIK','required|trim');
		$this->form_validation->set_rules('nohp','No Telepon','required|trim|max_length[12]');
		$this->form_validation->set_rules('tmp_lahir','Tempat Lahir','required|trim');
		$this->form_validation->set_rules('tgl_lahir','Tanggal lahir','required');
		$this->form_validation->set_rules('alamat','Alamat','required|trim');

		if ($this->form_validation->run() == FALSE) {

		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/daftarmerchant',$data);
		$this->load->view('templates/admin_footer');
		}else{
			$this->M_admin->tambahMerchant();
			$this->session->set_flashdata('flash' , 'Ditambahkan');
			redirect('Administrator/daftarmerchant');
		}
	}	
	public function info_merchant()
	{
		$data['judul'] = 'Info merchant';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/infomerchant',$data);
		$this->load->view('templates/admin_footer');		
	}
	public function uploadVideo()
	{
		$data['judul'] = 'Upload video';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/uploadVideo',$data);
		$this->load->view('templates/admin_footer');	
	}
	public function uploadReciter()
	{
		$data['judul'] = 'Upload Reciter';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/admin_header',$data);
		$this->load->view('templates/admin_sidebar',$data);
		$this->load->view('admin/uploadReciter',$data);
		$this->load->view('templates/admin_footer');	
	}
}