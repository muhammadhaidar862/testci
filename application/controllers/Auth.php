<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->form_validation->set_rules('username','Username','required|trim');
		$this->form_validation->set_rules('password','Password','required|trim');
	
		if ($this->form_validation->run() == false) {
			$data['judul'] = 'Login';
			$this->load->view('templates/auth_header',$data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');			
		}else{
			// validasinya success
			$this->_login();
		}
	}
	private function _login()
	{
		$username = $this->input->post('username',true);
		$password = $this->input->post('password',true);
		// SELECT * FROM tb_user WHERE username = $username
		$user = $this->db->get_where('user',
		['username' => $username])->row_array();
			// jika user ada
		if ($user) {
				$data = [
					'username' => $user['username'],
					'level' => $user['level']
				];
				// simpan ke dalam session
				$this->session->set_userdata($data);
				if ($user['level'] == 'Administrator') {
					redirect('Administrator');
				}else if($user['level'] == 'Ustadz'){
					redirect('Ustadz');
				}else if($user['level'] == 'AdminMerchant'){
					redirect('AdminMerchant');
				}else{
					redirect('Auth');
				}

		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">Email Tidak Terdaftar didatabase','</div>');
			redirect('auth');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
			$this->session->set_flashdata('message','<div class = "alert alert-success" role="alert">Anda telah keluar</div>');
			redirect('auth');	
	}

}