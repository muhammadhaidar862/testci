<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMerchant extends CI_Controller {
	public function index()
	{
		$data['judul'] = 'Merchant';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('merchant/index',$data);
		$this->load->view('templates/footer');
	}
	public function daftarMerchant()
	{
		$data['judul'] = 'Daftar Merchant';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('merchant/daftarMerchant',$data);
		$this->load->view('templates/footer');
	}
	public function infoAdminMerchant()
	{
		$data['judul'] = 'Info Merchant';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('merchant/infoAdminMerchant',$data);
		$this->load->view('templates/footer');
	}
}