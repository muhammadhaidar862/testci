<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustadz extends CI_Controller {
	public function index()
	{
		$data['judul'] = 'Ustadz';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('ustadz/index',$data);
		$this->load->view('templates/footer');
	}
	public function uploadvideo()
	{
		$data['judul'] = 'Ustadz';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('ustadz/video',$data);
		$this->load->view('templates/footer');
	}
	public function uploadreciter()
	{
		$data['judul'] = 'Ustadz';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('ustadz/reciter',$data);
		$this->load->view('templates/footer');
	}
	public function artikel_ustad()
	{
		$data['judul'] = 'Artikel';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('ustadz/artikel_ustad',$data);
		$this->load->view('templates/footer');
	}
	public function doa()
	{
		$data['judul'] = 'Doa';
		$data['user'] = $this->db->get_where('user',['username' => $this->session->userdata('username')])->row_array();
		$this->load->view('templates/header',$data);
		$this->load->view('ustadz/doa',$data);
		$this->load->view('templates/footer');
	}
}